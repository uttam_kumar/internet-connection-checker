//
//  ViewController.swift
//  InternetConnection
//
//  Created by Syncrhonous on 17/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import TTGSnackbar


class ViewController: UIViewController {

    @IBOutlet weak var connectionStatus: UILabel!
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    let diskStorage = DiskStatus()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        internetIdentifier()
        
    }
    
    func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }

    @IBOutlet weak var aboutStorage: UILabel!
    @IBAction func checkStorage(_ sender: UIButton) {
        var gb1 = Double(UIDevice.current.totalDiskSpaceInBytes)/(1024*1024*1024)
        var gb2 = UIDevice.current.usedDiskSpaceInBytes/(1024*1024*1024)
        var gb3 = UIDevice.current.freeDiskSpaceInBytes/(1024*1024*1024)
        print("totalDiskSpaceInBytes: \(gb1) GB")
        print("freeDiskSpace: \(gb3) GB")
        print("usedDiskSpace: \(gb2) GB")
        aboutStorage.text = String(gb2)
    }
    
    
    
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            self.connectionStatus.text = "connected"
            whenDeviceOnline()
        }else{
            whenDeviceOffline()
            self.connectionStatus.text = "not connected"
        }
    }
    
    
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Online"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    func whenDeviceOffline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "No Internet"
        snackbar.show()
    }
}

extension UIDevice {
    func MBFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }
    
    //MARK: Get String Value
    var totalDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var freeDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var usedDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var totalDiskSpaceInMB:String {
        return MBFormatter(totalDiskSpaceInBytes)
    }
    
    var freeDiskSpaceInMB:String {
        return MBFormatter(freeDiskSpaceInBytes)
    }
    
    var usedDiskSpaceInMB:String {
        return MBFormatter(usedDiskSpaceInBytes)
    }
    
    //MARK: Get raw value
    var totalDiskSpaceInBytes:Int64 {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value else { return 0 }
        return space
    }
    
    /*
     Total available capacity in bytes for "Important" resources, including space expected to be cleared by purging non-essential and cached resources. "Important" means something that the user or application clearly expects to be present on the local system, but is ultimately replaceable. This would include items that the user has explicitly requested via the UI, and resources that an application requires in order to provide functionality.
     Examples: A video that the user has explicitly requested to watch but has not yet finished watching or an audio file that the user has requested to download.
     This value should not be used in determining if there is room for an irreplaceable resource. In the case of irreplaceable resources, always attempt to save the resource regardless of available capacity and handle failure as gracefully as possible.
     */
    var freeDiskSpaceInBytes:Int64 {
        if #available(iOS 11.0, *) {
            if let space = try? URL(fileURLWithPath: NSHomeDirectory() as String).resourceValues(forKeys: [URLResourceKey.volumeAvailableCapacityForImportantUsageKey]).volumeAvailableCapacityForImportantUsage {
                return space ?? 0
            } else {
                return 0
            }
        } else {
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
                let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value {
                return freeSpace
            } else {
                return 0
            }
        }
    }
    
    var usedDiskSpaceInBytes:Int64 {
        return totalDiskSpaceInBytes - freeDiskSpaceInBytes
    }
    
}
